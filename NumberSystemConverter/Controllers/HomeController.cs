﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NumberSystemConverter.Models;
using NumberSystemConverter.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using NumberSystemConverter.ViewModels;

namespace NumberSystemConverter.Controllers
{
    public class HomeController : Controller
    {
        private readonly NumberSystemsContext _context;

        public HomeController(NumberSystemsContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var systems = await _context.NumberSystems.ToListAsync();

            var model = new ConverterViewModel
            {
                NumberSystems = systems
            };

            return View(model);
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
