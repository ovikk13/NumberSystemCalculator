﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NumberSystemConverter.Data;
using NumberSystemConverter.Models;
using NumberSystemConverter.ViewModels;

namespace NumberSystemConverter.Controllers
{
    public class NumberSystemsController : Controller
    {
        private readonly NumberSystemsContext _context;

        public NumberSystemsController(NumberSystemsContext context)
        {
            _context = context;
        }

        // GET: NumberSystems
        public async Task<IActionResult> Index()
        {
            return View(await _context.NumberSystems.ToListAsync());
        }

        // GET: NumberSystems/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var numberSystem = await _context.NumberSystems
                .SingleOrDefaultAsync(m => m.ID == id);
            if (numberSystem == null)
            {
                return NotFound();
            }

            return View(numberSystem);
        }

        // GET: NumberSystems/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: NumberSystems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Description,MapperKeys,MapperValues,Seperator")] NumberSystemViewModel numberSystemViewModel)
        {
            if (ModelState.IsValid)
            {
                var keys = numberSystemViewModel.MapperKeys;
                var values = numberSystemViewModel.MapperValues;

                // check mapping
                if (!NumberSystemMapper.ValidDict(keys, values))
                {
                    numberSystemViewModel.Error = "The mapping is invalid";
                    return View(numberSystemViewModel);
                }

                var mapperDict = keys.Zip(values, (k, v) => new { k, v }).ToDictionary(x => x.k, x => x.v);

                var numberSystem = new NumberSystem
                {
                    Name = numberSystemViewModel.Name,
                    Description = numberSystemViewModel.Description,
                    Mapper = mapperDict,
                    Seperator = numberSystemViewModel.Seperator
                };

                _context.Add(numberSystem);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            // reset error
            numberSystemViewModel.Error = null;

            return View(numberSystemViewModel);
        }

        // GET: NumberSystems/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var numberSystem = await _context.NumberSystems.SingleOrDefaultAsync(m => m.ID == id);
            if (numberSystem == null)
            {
                return NotFound();
            }

            var nsViewModel = new NumberSystemViewModel()
            {
                ID = numberSystem.ID,
                Name = numberSystem.Name,
                Description = numberSystem.Description,
                MapperKeys = numberSystem.Mapper.Dictionary.Keys,
                MapperValues = numberSystem.Mapper.Dictionary.Values,
                Seperator = numberSystem.Seperator
            };

            return View(nsViewModel);
        }

        // POST: NumberSystems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Description,MapperKeys,MapperValues,Seperator")] NumberSystemViewModel nsViewModel)
        {
            if (id != nsViewModel.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var keys = nsViewModel.MapperKeys;
                    var values = nsViewModel.MapperValues;

                    // check mapping
                    if (!NumberSystemMapper.ValidDict(keys, values))
                    {
                        nsViewModel.Error = "The mapping is invalid";
                        return View(nsViewModel);
                    }

                    var mapperDict = keys.Zip(values, (k, v) => new { k, v }).ToDictionary(x => x.k, x => x.v);

                    var numberSystem = new NumberSystem
                    {
                        ID = nsViewModel.ID,
                        Name = nsViewModel.Name,
                        Description = nsViewModel.Description,
                        Mapper = mapperDict,
                        Seperator = nsViewModel.Seperator
                    };

                    _context.Update(numberSystem);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NumberSystemExists(nsViewModel.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(nsViewModel);
        }

        // GET: NumberSystems/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var numberSystem = await _context.NumberSystems
                .SingleOrDefaultAsync(m => m.ID == id);
            if (numberSystem == null)
            {
                return NotFound();
            }

            return View(numberSystem);
        }

        // POST: NumberSystems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var numberSystem = await _context.NumberSystems.SingleOrDefaultAsync(m => m.ID == id);
            _context.NumberSystems.Remove(numberSystem);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NumberSystemExists(int id)
        {
            return _context.NumberSystems.Any(e => e.ID == id);
        }
    }
}
