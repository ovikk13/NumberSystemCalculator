﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NumberSystemConverter.Models;
using NumberSystemConverter.Data;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NumberSystemConverter.Controllers
{
    [Route("api/[controller]")]
    public class ConvertController : Controller
    {
        private readonly NumberSystemsContext _context;

        public ConvertController(NumberSystemsContext context)
        {
            _context = context;
        }

        // GET: api/values
        [HttpGet]
        public string Get(int fromSystemId, int toSystemId, string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                return Error("No input", 0);
            }

            var systems = _context.NumberSystems;

            var toSystem = systems.SingleOrDefault(sys => sys.ID == toSystemId);
            var fromSystem = systems.SingleOrDefault(sys => sys.ID == fromSystemId);

            var converterModel = new ConverterModel()
            {
                FromSystem = fromSystem,
                ToSystem = toSystem,
                Input = input
            };

            try
            {
                var result = converterModel.Calculate();
                return Success(result);
            }
            catch (ArgumentException)
            {
                return Error("Invalid input", 1);
            }
        }

        protected string Error(string error, int type)
        {
            return "{\"error\":\"" + error + "\",\"type\":" + type + "}";
        }

        protected string Success(string value)
        {
            return "{\"result\":\"" + value + "\"}";
        }
    }
}
