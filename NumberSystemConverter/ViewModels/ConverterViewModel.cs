﻿
using Microsoft.AspNetCore.Mvc.Rendering;
using NumberSystemConverter.Models;
using System.Collections.Generic;

namespace NumberSystemConverter.ViewModels
{
    public class ConverterViewModel
    {
        public List<NumberSystem> NumberSystems { get; set; }

        public string Input { get; set; }

        public int FromSystemId { get; set; }
        public int ToSystemId { get; set; }
    }
}
