﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NumberSystemConverter.ViewModels
{
    public class NumberSystemViewModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public IEnumerable<string> MapperKeys { get; set; }

        [Required]
        public IEnumerable<int> MapperValues { get; set; }

        public string Seperator { get; set; }

        public int ID { get; set; }
        public string Error { get; set; }
    }
}
