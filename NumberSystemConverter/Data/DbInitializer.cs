﻿using Newtonsoft.Json;
using NumberSystemConverter.Models;
using System.Collections.Generic;
using System.Linq;

namespace NumberSystemConverter.Data
{
    public static class DbInitializer
    {
        public static void Initialize(NumberSystemsContext context)
        {
            context.Database.EnsureCreated();

            if (context.NumberSystems.Any())
                return; // no need to create db

            var defaultSystems = new NumberSystem[]
            {
                new NumberSystem
                {
                    Name = "Binary",
                    Description = "Number system used in computers",
                    MapperJSON = JsonConvert.SerializeObject(
                        new Dictionary<string, int>
                        {
                            { "0", 0 },
                            { "1", 1 }
                        }
                    )
                }
            };

            foreach (NumberSystem ns in defaultSystems)
                context.NumberSystems.Add(ns);
            context.SaveChanges();
        }
    }
}
