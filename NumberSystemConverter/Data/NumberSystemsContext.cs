﻿using Microsoft.EntityFrameworkCore;
using NumberSystemConverter.Models;

namespace NumberSystemConverter.Data
{
    public class NumberSystemsContext : DbContext
    {
        public DbSet<NumberSystem> NumberSystems { get; set; }

        public NumberSystemsContext(DbContextOptions<NumberSystemsContext> options) : base(options)
        { }
    }
}
