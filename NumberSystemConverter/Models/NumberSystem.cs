﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading;

namespace NumberSystemConverter.Models
{
    public class NumberSystem
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MapperJSON { get; set; }
        public string Seperator { get; set; }

        [NotMapped]
        public SemaphoreSlim Lock = new SemaphoreSlim(1);

        [NotMapped]
        private NumberSystemMapper _mapper = null;
        [NotMapped]
        public NumberSystemMapper Mapper
        {
            get
            {
                if (_mapper != null)
                    return _mapper;
                else if (MapperJSON == null)
                    return null;
                else
                {
                    Lock.Wait();
                    // if the _mapper has been created by another thread, use that instead
                    if (_mapper != null)
                        return _mapper;

                    _mapper = NumberSystemMapper.FromJSON(MapperJSON);

                    Lock.Release();
                    return _mapper;
                }
            }
            set
            {
                if (value != _mapper)
                {
                    var serialized = value.JSON();
                    if (serialized != MapperJSON)
                        MapperJSON = serialized;
                }
            }
        }
    }

    public class NumberSystemMapper
    {
        public Dictionary<string, int> Dictionary { get; private set; }
        public int Base => Dictionary.Count;

        protected NumberSystemMapper(Dictionary<string, int> mapper)
        {
            Dictionary = mapper;
        }

        public int Decimal(string input)
        {
            return Dictionary[input];
        }

        public string System(int deci)
        {
            var reversed = Dictionary.ToDictionary(d => d.Value, d => d.Key);
            return reversed[deci];
        }

        public string JSON()
        {
            return JsonConvert.SerializeObject(Dictionary);
        }

        public static NumberSystemMapper Create(Dictionary<string, int> mapper)
        {
            if (mapper != null)
                return new NumberSystemMapper(mapper);
            else
                return null;
        }

        public static NumberSystemMapper FromJSON(string json)
        {
            var mapper = JsonConvert.DeserializeObject<Dictionary<string, int>>(json);
            return Create(mapper);
        }

        public static implicit operator NumberSystemMapper(Dictionary<string, int> mapper)
        {
            return Create(mapper);
        }

        public static implicit operator Dictionary<string, int>(NumberSystemMapper mapper)
        {
            return mapper.Dictionary;
        }

        public static bool ValidDict(IEnumerable<string> keys, IEnumerable<int> values)
        {
            // smallest system has a base of 2
            if (keys.Count() < 2 || values.Count() < 2)
                return false;

            // check if there is a decimal value from 0 and up
            values.OrderBy(x => x);
            var counter = 0;
            foreach (var value in values)
            {
                if (value != counter)
                    return false;
                ++counter;
            }

            // passed all checks
            return true;
        }
    }
}
