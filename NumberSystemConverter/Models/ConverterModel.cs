﻿
using System;
using System.Linq;
using System.Text;

namespace NumberSystemConverter.Models
{
    public class ConverterModel
    {
        public string Input { get; set; }
        public NumberSystem FromSystem { get; set; }
        public NumberSystem ToSystem { get; set; }

        public string Calculate()
        {
            var deci = SystemToDecimal(Input, FromSystem);
            string result = DecimalToSystem(deci, ToSystem);
            return result;
        }

        public static int SystemToDecimal(string input, NumberSystem fromSystem)
        {
            if (String.IsNullOrEmpty(input) || fromSystem == null)
                return 0;

            var inputArray = String.IsNullOrEmpty(fromSystem.Seperator)
                ? input.ToCharArray().Select(c => c.ToString()).ToArray()
                : input.Split(fromSystem.Seperator);

            var mapper = fromSystem.Mapper;
            var keys = mapper.Dictionary.Keys;

            // check if inputArray contains invalid elements
            if (inputArray.Except(keys).Any())
                throw new ArgumentException("Input contains invalid elements");

            // reverse the inputArray, we want to start at the end
            Array.Reverse(inputArray);

            var result = 0;
            var muliplier = 1;
            foreach (var num in inputArray)
            {
                result += mapper.Decimal(num) * muliplier;

                // increase multiplier for each iteration
                muliplier *= fromSystem.Mapper.Base;
            }

            return result;
        }

        public static string DecimalToSystem(int deci, NumberSystem toSystem)
        {
            if (toSystem == null)
                throw new ArgumentNullException("Cannot convert to a null system!");

            var mapper = toSystem.Mapper;

            // if deci is zero, we can just return zero
            if (deci == 0)
            {
                var zero = mapper.System(deci);
                return zero;
            }

            var result = new StringBuilder();
            var multiplier = 1;
            var lastMultiplier = -1;
            do
            {
                if (deci == 0 && lastMultiplier != 1)
                {
                    // fill in the remaining zeros
                    while (lastMultiplier != 1)
                    {
                        var zero = mapper.System(deci);
                        result.Append(zero);
                        lastMultiplier /= mapper.Base;
                    }
                }
                else if (multiplier < deci)
                {
                    // increase multiplier
                    multiplier *= mapper.Base;
                }
                else
                {
                    // we found max, go down by one multiplier
                    // unless the multiplier is the same as the leftover
                    if (multiplier != deci)
                        multiplier /= mapper.Base;

                    // fill in zeros
                    if (result.Length > 0)
                    {
                        // since we are supposed to decrease multiplier by one, we
                        // want to skip one zero
                        lastMultiplier /= mapper.Base;

                        // we have already written something, so we should fill
                        // in the zeros where we need to
                        while (lastMultiplier > multiplier)
                        {
                            var zero = mapper.System(0);
                            result.Append(zero);
                            lastMultiplier /= mapper.Base;
                        }
                    }

                    // calculate leftovers
                    var leftover = deci % multiplier;

                    // get the decimal value of the current place
                    // we want to remove the leftovers from the decimal so
                    // the divition result in a whole integer
                    var deciValue = (deci - leftover) / multiplier;

                    // use the deciValue in the mapper to get the respective
                    // string for that value
                    string value = mapper.System(deciValue);

                    // add the resulting char to the result
                    result.Append(value);

                    // repeat the process, but using the leftovers. The
                    // rest has already been accounted for
                    deci = leftover;
                    lastMultiplier = multiplier;
                    multiplier = 1;
                }
            }
            while (deci != 0 || lastMultiplier != 1);

            return result.ToString();
        }
    }
}
