﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;

namespace NumberSystemConverter.Models
{
    public class MapperModel
    {
        public const string RANGEKEY = "...";
        public const string COUNTMACRO = "{COUNT}";

        public static readonly string ORDER = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        public List<KeyValuePair<string, string>> Input { get; set; }

        public string Generate()
        {
            var mapper = new Dictionary<string, int>();

            foreach (var inputVar in Input)
            {
                if (IsRange(inputVar))
                {
                    var results = FromRange(inputVar);
                    foreach (var result in results)
                        mapper.Add(result.Key, result.Value);
                }
                else
                {
                    var key = inputVar.Key;
                    var value = inputVar.Value;

                    if (!IsNumber(value))
                        throw new ArgumentException("Decimal value must be number!");

                    mapper.Add(key, int.Parse(value));
                }
            }

            return JsonConvert.SerializeObject(mapper);
        }

        private Dictionary<string, int> FromRange(KeyValuePair<string, string> input)
        {
            var key = input.Key;
            var value = input.Value;

            var keyParts = key.Split(RANGEKEY);
            if (keyParts.Length != 2)
                throw new ArgumentException("Range cannot contain more than a from and to part!");
            var keyFrom = keyParts[0];
            var keyTo = keyParts[1];

            var keys = Populate(null, keyFrom, keyTo);

            var values = new List<int>();
            if (value != COUNTMACRO)
            {
                var valueParts = value.Split(RANGEKEY);
                if (valueParts.Length != 2)
                    throw new ArgumentException("Range cannot contain more than a from and to part!");
                var valueFrom = keyParts[0];
                var valueTo = keyParts[1];

                if (!IsNumber(valueFrom) || !IsNumber(valueTo))
                    throw new ArgumentException("Decimal value cannot contain numbers!");

                values = Populate(null, valueFrom, valueTo).Select(x => int.Parse(x)).ToList();
            }
            else
            {
                for (var i = 0; i <= keys.Count; ++i)
                    values.Add(i);
            }

            var result = keys.Zip(values, (k, v) => new { k, v }).ToDictionary(x => x.k, x => x.v);
            return result;
        }

        private List<string> Populate(List<string> prefixes, string remainingFrom, string remainingTo)
        {
            // make sure prefixes is valid
            if (prefixes == null)
                prefixes = new List<string>();
            if (prefixes.Count <= 0)
                prefixes.Add("");

            if (String.IsNullOrEmpty(remainingFrom) || String.IsNullOrEmpty(remainingTo))
                return null;

            var result = new List<string>();

            var from = remainingFrom[0];
            var to = remainingTo[0];

            if (ORDER.IndexOf(from) > ORDER.IndexOf(to))
                throw new ArgumentException("From cannot be before to!");

            var range = Enumerable.Range(from, ORDER.IndexOf(to) + 1).Select(x => ((char)x).ToString());

            foreach (var prefix in prefixes)
            {
                foreach (var value in range)
                {
                    var toAdd = prefix + value;
                    result.Add(toAdd);
                }
            }

            remainingFrom = remainingFrom.Remove(0, 1);
            remainingTo = remainingTo.Remove(0, 1);

            if (remainingFrom.Length > 0 && remainingTo.Length > 0)
                return Populate(result, remainingFrom, remainingTo);
            else
                return result;
        }

        public bool IsRange(KeyValuePair<string, string> value)
        {
            var input = value.Key;
            var number = value.Value;
            return input.Contains(RANGEKEY) && (number.Contains(RANGEKEY) || number == COUNTMACRO); 
        }

        public bool IsNumber(string input)
        {
            return !int.TryParse(input, out var unused);
        }
    }
}
